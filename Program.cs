﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Websocket.Client;

namespace binapi
{
    class Program
    {
        private const String API_URL = "https://api.binance.com/api/v3/exchangeInfo?symbol=";
        private const String WSS_URL = "wss://stream.binance.com:9443/ws";
        private const int MESSAGE_LIMITS = 1000;
        static void Main(string[] args)
        {
            if(args.Length != 2)
            {
                Console.WriteLine("usage: option parameter");
            }
            else
            {
                switch(args[0].ToLowerInvariant())
                {
                    #region INFO
                    case "info":
                        {
                            try
                            {
                                HttpClient client = new HttpClient();
                                var response = client.GetAsync($"{API_URL}{args[1].ToUpperInvariant()}");
                                if (response.Result.IsSuccessStatusCode)
                                {
                                    var rs = response.Result.Content.ReadAsStringAsync();
                                    if (!String.IsNullOrEmpty(rs.Result))
                                    {
                                        Console.WriteLine(rs.Result);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine($"{response.Result.RequestMessage.RequestUri} returns {response.Result.ReasonPhrase}");
                                }
                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine("ERROR: " + ex.Message);
                            }
                            break;
                        }
                    #endregion
                    #region SUBCRIPTION
                    case "subscribe":
                        {
                            int msgCount = 0;
                            DateTime startTime = DateTime.Now;
                            try
                            {
                                ManualResetEvent exitEvent = new ManualResetEvent(false);

                                using (WebsocketClient client = new WebsocketClient(new Uri(WSS_URL)))
                                {
                                    client.ReconnectTimeout = TimeSpan.FromSeconds(30);
                                    client.ReconnectionHappened.Subscribe(info =>
                                    {
                                        Console.WriteLine("Reconnection happened");
                                    });
                                    client.MessageReceived.Subscribe(msg =>
                                    {
                                        Console.WriteLine("Message received: " + msg);
                                        if (msg.MessageType == System.Net.WebSockets.WebSocketMessageType.Text)
                                        {
                                            if(msgCount == 0)
                                            {
                                                startTime = DateTime.Now;
                                            }
                                            msgCount++;
                                            if(msgCount == MESSAGE_LIMITS)
                                            {
                                                string unScr = $"{{\"method\": \"UNSUBSCRIBE\",\"params\":[\"{args[1]}@aggTrade\"],\"id\": 1}}";
                                                client.Send(unScr);
                                                Console.WriteLine($"Duration is {DateTime.Now.Subtract(startTime)}");
                                                client.Stop(System.Net.WebSockets.WebSocketCloseStatus.NormalClosure, "exit");
                                                exitEvent.Set();
                                            }
                                        }
                                    });
                                    client.Start();
                                    string src = $"{{\"method\": \"SUBSCRIBE\",\"params\":[\"{args[1]}@aggTrade\"],\"id\": 1}}";
                                    client.Send(src);
                                    exitEvent.WaitOne();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("ERROR: " + ex.ToString());
                            }
                            break;
                        }
                    #endregion
                    default:
                        {
                            Console.WriteLine("wrong option");
                            break;
                        }
                }
            }
            Console.ReadLine();
        }
    }
}
